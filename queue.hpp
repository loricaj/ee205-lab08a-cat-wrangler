///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file queue.hpp
/// @version 1.0
///
/// A generic Queue collection class.
///
/// @author Joshua Lorica <loricaj@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   4/13/21
///////////////////////////////////////////////////////////////////////////////

#include "node.hpp"
#include "list.hpp"

#pragma once

class Queue {
protected:
	DoubleLinkedList list = DoubleLinkedList();

public:
	inline const bool empty() const { return list.empty(); };

   inline unsigned int size() const { return list.size(); };
   
   inline void push_front( Node* newNode ) { return list.push_front( newNode ); };
   
   inline Node* pop_back() { return list.pop_back(); };
   
   inline Node* get_first() const { return list.get_first(); };
   
   inline Node* get_next( const Node* currentNode ) const { return list.get_next( currentNode ); };

}; // class Queue
