///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.hpp
/// @version 1.0
///
/// 
///
/// @author Joshua Lorica <loricaj@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   4/12/21
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "node.hpp"

class DoubleLinkedList {

protected:

   Node* head = nullptr;
   Node* tail = nullptr;
   unsigned int count = 0;

public:

   const bool empty() const;  // Return true if the list is empty. false if there's anything in the list
   
   void push_front( Node* newNode );   // Add newNode to the front of the list
   void push_back( Node* newNode );   // Add newNode to the back of the list

   Node* pop_front();   // Remove a node from the front of the list. If the list is already empty, return nullptr
   Node* pop_back();   // Remove a node from the back of the list. If the list is already empty, return nullptr
   
   Node* get_first() const;   // Return the very first node from the list. Don't make any changes to the list
   Node* get_last() const;   // Return the very last node from the list. Don't make any changes to the list

   Node* get_next( const Node* currentNode ) const;   // Return the node immediately following currentNode
   Node* get_prev( const Node* currentNode ) const;   // Return the node immediately before currentNode

   inline unsigned int size() const { return count; };   // Return the number of nodes in the list

   void swap( Node* node1, Node* node2 );

   const bool isSorted() const; // This funciton depends on Node's < operator
   void insertionSort();         // This function depends on Node's < operator

}; // end of DoubleLinkedList class
