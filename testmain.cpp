///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file maintest.cpp
/// @version 2.0
///
/// Unit test for this lab
///
/// @author Joshua Lorica <loricaj@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   4/12/21
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cassert>

#include "cat.hpp"
#include "list.hpp"

using namespace std;

void printList();

int main() {

   cout << "Hello World!" << endl;

   Node node;  // Instantiate a node

   DoubleLinkedList list;  // Instantiate a DoubleLinkedList
   
   Cat::initNames();

   list = DoubleLinkedList();
	
	list.push_front( Cat::makeCat() );
	list.push_front( Cat::makeCat() );
	list.push_front( Cat::makeCat() );
	
   cout << list.size() << endl;

   for( Cat* cat = (Cat*)list.get_last(); cat != nullptr; cat = (Cat*)list.get_prev(cat)) 
      cout << cat->getInfo() << endl;

   list.pop_back();
   cout << "pop back" << endl;

   cout << list.size() << endl;

   for( Cat* cat = (Cat*)list.get_last(); cat != nullptr; cat = (Cat*)list.get_prev(cat)) 
      cout << cat->getInfo() << endl;

   list.pop_front();
   cout << "pop front" << endl;

   for( Cat* cat = (Cat*)list.get_last(); cat != nullptr; cat = (Cat*)list.get_prev(cat)) 
      cout << cat->getInfo() << endl;

   cout << list.size() << endl;

   cout << "Add 4 more Cats, 2 push back, 1 push front" << endl;

   
	list.push_back( Cat::makeCat() );
	list.push_back( Cat::makeCat() );
	list.push_back( Cat::makeCat() );
	list.push_front( Cat::makeCat() );

   cout << list.size() << endl;

   for( Cat* cat = (Cat*)list.get_last(); cat != nullptr; cat = (Cat*)list.get_prev(cat)) 
      cout << cat->getInfo() << endl;


   cout << "Swap head and tail" << endl;

	list.swap( list.get_first(), list.get_last() );

   for( Cat* cat = (Cat*)list.get_last(); cat != nullptr; cat = (Cat*)list.get_prev(cat)) 
      cout << cat->getInfo() << endl;

   cout << "Sort List" << endl;

   list.insertionSort();

   for( Cat* cat = (Cat*)list.get_last(); cat != nullptr; cat = (Cat*)list.get_prev(cat)) 
      cout << cat->getInfo() << endl;

}


