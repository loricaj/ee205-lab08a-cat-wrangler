///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.cpp
/// @version 1.0
///
/// 
///
/// @author Joshua Lorica <loricaj@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   4/12/21
///////////////////////////////////////////////////////////////////////////////


#include "list.hpp"
#include "node.hpp"
#include <iostream>
#include <list>
#include <cassert>
//#include "cat.hpp"

using namespace std;

// Return true if the list is empty, false if there’s anything in the list
const bool DoubleLinkedList::empty() const {
   if ( head == nullptr )
      return true;
   else
      return 0;
}

// Add newNode to the front of the list
void DoubleLinkedList::push_front( Node* newNode ) {
   count++;
   if( head == nullptr ) {
      head = newNode;
      tail = newNode;
      newNode->prev = nullptr;
      newNode->next = nullptr;
   }
   else{
      newNode->next = head;
      newNode->prev = nullptr;
      head->prev = newNode;
      head = newNode;
   }
}

void DoubleLinkedList::push_back( Node* newNode ) {
   count++;
   newNode->prev = tail;
   tail = newNode;
   newNode->next = nullptr;
}

// Remove a node from the front of the list. If the list is already empty, return nullptr
Node* DoubleLinkedList::pop_front() {
   count--;
   if ( head == nullptr ) {
      return nullptr;
   }

   Node *temp = head;
   head = head->next;
   head->prev = nullptr;

   return temp;
}

Node* DoubleLinkedList::pop_back() {
   count--;
   if ( tail == nullptr ) {
      return nullptr;
   }
   if ( head == tail ) {
      Node *temp = tail;
      tail = nullptr;
      head = nullptr;
      return temp;
   }
   else {
      Node *temp = tail;
      tail = tail->prev;
      tail->next = nullptr;
      temp->prev = nullptr;
      return temp;
   }
}

// Return the very first node from the list. Don’t make any changes to the list.
Node* DoubleLinkedList::get_first() const {
   return head;
}

Node* DoubleLinkedList::get_last() const {
   return tail;
}

// Return the node immediately following currentNode
Node* DoubleLinkedList::get_next( const Node* currentNode ) const {
   return currentNode->next;
}

Node* DoubleLinkedList::get_prev( const Node* currentNode ) const {
   return currentNode->prev;
}

void DoubleLinkedList::swap(Node* node1, Node* node2) {
   assert( !empty() );

   assert( node1 != nullptr );
   assert( node2 != nullptr );

   //assert( isIn( node1 ));
   //assert( isIn( node2 ));
   
   if( node1 == node2 )
      return;

   Node* node1_left  = node1->prev;
   Node* node1_right = node1->next;
   Node* node2_left  = node2->prev;
   Node* node2_right = node2->next;

   bool isAdjoining = (node1_right == node2);

   if( !isAdjoining )
      node1->prev = node2->prev;
   else
      node1->prev = node2;

   if( node1 != head ) {
      node1_left->next = node2;
      node2->prev = node1_left;
   }
   else {
      head = node2;
      node2->prev = nullptr;
   }

   if( !isAdjoining )
      node2->next = node1_right;
   else
      node2->next = node1;

   if( node2!= tail ) {
      node2_right->prev = node1;
      node1->next = node2_right;
   }
   else {
      tail = node1;
      node1->next = nullptr;
   }

   if( !isAdjoining ) {
      node1_right->prev = node2;
      node2_left->next = node1;
   }
}

const bool DoubleLinkedList::isSorted() const {
   if( count <= 1 )  
      return true;
   for( Node* i = head; i->next != nullptr; i=i->next ) {
      if( *i > *i->next )
         return false;
   }
   return true;
}

void DoubleLinkedList::insertionSort() {
   Node* current = nullptr;
   Node* index = nullptr;

   if(head == nullptr) {
      return;
   }
   else{
      //current will point to the right of head
      for(current = head->next; current!=nullptr; current = current->next) {
         //index will point to node left of curent
         for(index = current->prev; index != nullptr; index = index->prev) {
            if(current < index){
               swap(current,index); 
               index = current;
            }
         }
      }
   }

}
