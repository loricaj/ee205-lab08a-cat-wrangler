///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file queueSim.cpp
/// @version 2.0
///
/// A simulatior for a Queue
///
/// @author Mark Nelson <marknels@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   30 Mar 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>  // For cout
#include <random>    // For random_device
#include <thread>    // For sleep_for
#include <iomanip>   // For setfill

#include "queue.hpp"

using namespace std;

// Alpha extends Node... and we will carry a single character, 'a' through 'z' in it
class Alpha : public Node {
public:
	char memberChar;

	static char staticChar;

	Alpha() { memberChar = staticChar++ ; if( staticChar > 'z' ) staticChar = 'a'; }
};

char Alpha::staticChar = 'a';


constinit const int PAR = 4;       // The average size of the queue
constinit const int STRENGTH = 1;  // The k-factor that brings the queue back to PAR

random_device randomDevice;        // Seed with a real random value, if available
mt19937_64 RNG( randomDevice() );  // Define a modern Random Number Generator
uniform_int_distribution<>  queueRNG (0, PAR * 2);


int main() {
	cout << "Queue simulator" << endl;
	cout << "     in    queue       out" << endl;
	cout << "     ==    ========    ===" << endl;

	Queue queue = Queue();
	Alpha* oldAlpha = nullptr;;

	// This is a unique way to iterate over a range of integers using C20
	for(int i : std::views::iota(1, 10000) ) {
		int offset = (queue.size() - PAR) * STRENGTH;  // Close to 0 is ideal.  Positive means throttle down.  Negative means throttle up.
		bool addToQueue = (queueRNG( RNG ) < (PAR - offset)) ? true : false;

		// This is one way to format numbers in C++
		cout << setw(4) << setfill('0') << i << ": " ;

		if( addToQueue ) {
			Alpha* newAlpha = new Alpha();
			cout << newAlpha->memberChar << " >> ";
			queue.push_front( newAlpha );
		} else {
			cout << "     ";
			oldAlpha = (Alpha*) queue.pop_back();
		}

		// Print the queue
		Node* current = queue.get_first();
		while( current != nullptr ) {
			cout << ((Alpha*)current)->memberChar ;
			current = queue.get_next( current );
		}

		// Pad queue with spaces
		int padSize = PAR*2-queue.size();
		for( int j = 0 ; j < padSize ; j++ ) {
			cout << ' ';
		}

		if( !addToQueue && oldAlpha != nullptr ) {
			cout << " >> " << oldAlpha->memberChar ;
		}

		cout << endl;

		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
}

